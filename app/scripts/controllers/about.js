'use strict';

/**
 * @ngdoc function
 * @name otakunotApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the otakunotApp
 */
angular.module('otakunotApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
