'use strict';

/**
 * @ngdoc function
 * @name otakunotApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the otakunotApp
 */
angular.module('otakunotApp')
  .controller('communityCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
